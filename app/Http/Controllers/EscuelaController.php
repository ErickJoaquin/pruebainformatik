<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Estudiante;
use App\Asignatura;
use App\estudiante_asignatura;
use App\Calificacion;
class EscuelaController extends Controller
{
    public function Estudiantes(){

    	return view('estudiantes/registrarEstudiantes');
    }
    public function registrarEstudiantes(Request $request){
    	Estudiante::create([
    		'nombre' => $request['nombre'],
    		'apellido' => $request['apellido'],
    		'email' => $request['email'],
    		'telefono' => $request['telefono'],
    		'matricula' => $request['matricula'],
    	]);
    	return redirect('listarEstudiantes');
    }

    public function listarEstudiantes(){
    	$estudiantes = Estudiante::all();
    	return view('estudiantes/listarEstudiantes',['estudiantes' => $estudiantes]);
    }

    public function asignatura(){
    	$asignaturas = Asignatura::all();
    	$idEstudiante = $_GET['id'];
    	return view('estudiantes/asignarAsignatura',['asignaturas' => $asignaturas,'idEstudiante' => $idEstudiante]);
    }
    public function asignarAsignatura(Request $request){
    	$asignatura = Asignatura::where('nombre','=',$request['asignatura'])->value('idAsignatura');
    	$est_asign = estudiante_asignatura::where('idAsignatura','=',$asignatura)->where('idEstudiante','=',$request['idEstudiante'])->get();
    	if ($est_asign == null){
			    	estudiante_asignatura::create([
    		'idEstudiante' => $request['idEstudiante'],
    		'idAsignatura' => $asignatura
    	]);    		
    	}
    	else{
    		echo "Asignatura ya asignada";
    	}

    }

    public function calificaciones(){
    	$idEstudiante = $_GET['id'];
    	$estudiante = Estudiante::where('id','=',$idEstudiante)->get();
    	$calificacion = Calificacion::where('idEstudiante','=',$idEstudiante)
    	->join('asignaturas','asignaturas.idAsignatura','=','calificaciones.idAsignatura')->get();
    	return view('estudiantes/calificaciones',['calificacion' => $calificacion,'estudiante'=> $estudiante]);
    }
    public function estudianteAsignatura(){
    	$idEstudiante = $_GET['id'];
    	$estudiante = Estudiante::where('id','=',$idEstudiante)->get();
    	$asignaturas = estudiante_asignatura::where('idEstudiante','=',$idEstudiante)
    	->join('asignaturas','asignaturas.idAsignatura','=','estudiante_asignatura.idAsignatura')->get();
    	return view('estudiantes/estudianteAsignatura',['estudiante' => $estudiante, 'asignaturas' => $asignaturas]);
    }

    public function cancelarNota(){
    	$idEstudiante =$_GET['idEstudiante'];
    	$idAsignatura =$_GET['idAsignatura'];
    	$calificacion=Calificacion::where('idEstudiante', '=', $idEstudiante)->where('idAsignatura', '=', $idAsignatura)->first();
    	$calificacion->delete();
    }
    public function cambiarNote(Request $request){
    	$idEstudiante =$request['idEstudiante'];
    	$idAsignatura =$request['idAsignatura'];
    	$calificacion=Calificacion::where('idEstudiante', '=', $idEstudiante)->where('idAsignatura', '=', $idAsignatura)->value('idCalificacion');
    	$calificacion = Calificacion::find($calificacion);
    	$calificacion->calificacion = $request['calificacion'];
    	$calificacion->save();
    }
    public function asignarNota(){
    	$idEstudiante = $_GET['id'];
    	$idAsignatura = $_GET['idAsignatura'];
    	return view('estudiantes/asignarNota',['idEstudiante' => $idEstudiante, 'idAsignatura' => $idAsignatura]);
    }
    public function asignarNote(Request $request){
    	$calificacion = Calificacion::where('idAsignatura','=',$request['idAsignatura'])->value('idAsignatura');
    	if($calificacion == null){
    	Calificacion::create([
    		'idEstudiante' => $request['idEstudiante'],  
    		'idAsignatura' => $request['idAsignatura'],
    		'calificacion' => $request['calificacion']

    	]);
    	}
    	else{
    		echo "Nota ya asignada";
    	}

    }

    public function cambiarNota(){
    	$idEstudiante = $_GET['idEstudiante'];
    	$idAsignatura = $_GET['idAsignatura'];
    	return view('estudiantes/cambiarNota',['idEstudiante'=> $idEstudiante , 'idAsignatura' => $idAsignatura]);
    }
    public function estudiantesCali(Request $request){
    	return view('estudiantes/estudiantesCali');
    }   
    public function estudiantesCalificaciones(Request $request){
    	
    	$idEstudiante = Estudiante::where('matricula','=',$request['matricula'])->value('id');
    	$calificaciones = Calificacion::where('idEstudiante','=',$idEstudiante)
    	->join('asignaturas','asignaturas.idAsignatura','=','calificaciones.idAsignatura')->get();
    	return view('estudiantes/estudianteCalificaciones',['calificaciones' => $calificaciones]);
    }
}
