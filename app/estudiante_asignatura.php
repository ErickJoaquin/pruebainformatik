<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class estudiante_asignatura extends Model
{
    protected $table = "estudiante_asignatura";
    protected $fillable = ['idEstudiante','idAsignatura'];
    public $timestamps = false;
}
