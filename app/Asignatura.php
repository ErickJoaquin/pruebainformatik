<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Asignatura extends Model
{
    protected $tables = "asignaturas";
    protected $primaryKey = "idAsignatura";
    protected $fillable = ['nombre'];
    public $timestamps = false;

}
