<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Calificacion extends Model
{
    protected $table = "calificaciones";
    protected $primaryKey = "idCalificacion";
    protected $fillable = ['idEstudiante','idAsignatura','calificacion'];
    public $timestamps = false;
}
