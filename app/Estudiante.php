<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Estudiante extends Model
{
    protected $table = "estudiantes";
    protected $primaryKey = "idEstudiantes";
    protected $fillable = ['nombre','apellido','email','telefono','matricula'];
    public $timestamps = false;
}
