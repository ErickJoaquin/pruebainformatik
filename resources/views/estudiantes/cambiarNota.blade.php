@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Cambiar calificacion</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ URL::to('/cambiarNote')}}">
                        {{ csrf_field() }}
                        <input type="hidden" name="idEstudiante" value="{{$idEstudiante}}">
                        <input type="hidden" name="idAsignatura" value="{{$idAsignatura}}">
                        <div class="form-group{{ $errors->has('calificacion') ? ' has-error' : '' }}">
                            <label for="calificacion" class="col-md-4 control-label">Calificacion</label>

                            <div class="col-md-6">
                                <input id="calificacion"  type="text" class="form-control" name="calificacion" value="{{ old('calificacion') }}" required autofocus>
                                
                                @if ($errors->has('calificacion'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('calificacion') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Asignar
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
