@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Buscar calificaciones</div>
                <div class="panel-body">
                    
                    <form class="form-horizontal" method="POST" action="{{ URL::to('/estudiantesCalificaciones')}}">
                        {{ csrf_field() }}
                        <div class="form-group{{ $errors->has('matricula') ? ' has-error' : '' }}">
                            <label for="matricula" class="col-md-4 control-label">Matriucula</label>

                            <div class="col-md-6">
                                <input id="matricula"  type="text" class="form-control" name="matricula" value="{{ old('matricula') }}" required autofocus>
                                
                                @if ($errors->has('matricula'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('matricula') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Buscar
                                </button>
                            </div>
                        </div>
                    </form>
                    

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
