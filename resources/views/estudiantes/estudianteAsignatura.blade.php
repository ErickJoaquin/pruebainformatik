@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Listado estudiantes</div>
                <div class="panel-body">
                    @foreach ($estudiante as $estudiante)
                    @endForeach
                    <h3>Asignaturas de: {{$estudiante->nombre}} {{$estudiante->apellido}}</h3>
                    <table class="table">
                      <thead>
                        <tr>
                          <th scope="col">Asignatura</th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach($asignaturas as $asignatura)
                            <tr>
                              <td scope="row">{{$asignatura->nombre}}</td>
                              <td><a href="{{ URL::to('/asignarNota')}}?id={{$estudiante->id}}&idAsignatura={{$asignatura->idAsignatura}}">Asignar nota</a></td>
                            </tr>
                            <tr>
                        @endForeach
                      </tbody>
                    </table>
                    

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
