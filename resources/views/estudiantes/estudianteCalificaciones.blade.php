@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Listado estudiantes</div>
                <div class="panel-body">
                    
                    <table class="table">
                      <thead>
                        <tr>
                          <th scope="col">Asignatura</th>
                          <th scope="col">Nota</th>

                        </tr>
                      </thead>
                      <tbody>
                        @foreach($calificaciones as $calificacion)
                            <tr>
                              <th scope="row">{{$calificacion->nombre}}</th>
                              <td>{{$calificacion->calificacion}}</td>
                            </tr>
                        @endForeach
                      </tbody>
                    </table>
                    

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
