@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Listado estudiantes</div>
                <div class="panel-body">
                    
                    <table class="table">
                      <thead>
                        <tr>
                          <th scope="col">ID</th>
                          <th scope="col">Nombre</th>
                          <th scope="col">Apellido</th>
                          <th scope="col">Email</th>
                          <th scope="col">Telefono</th>
                          <th scope="col">Matricula</th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach($estudiantes as $estudiante)
                            <tr>
                              <th scope="row">{{$estudiante->id}}</th>
                              <td>{{$estudiante->nombre}}</td>
                              <td>{{$estudiante->apellido}}</td>
                              <td>{{$estudiante->email}}</td>
                              <td>{{$estudiante->telefono}}</td>
                              <td>{{$estudiante->matricula}}</td>
                              <td><a href="{{ URL::to('/asignarAsignatura')}}?id={{$estudiante->id}}">Asignar Asignatura</a></td>
                              <td><a href="{{ URL::to('/calificaciones')}}?id={{$estudiante->id}}">Calificaciones</a></td>
                              <td><a href="{{ URL::to('/estudianteAsignatura')}}?id={{$estudiante->id}}">Asignaturas</a></td>
                            </tr>
                            <tr>
                        @endForeach
                      </tbody>
                    </table>
                    

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
