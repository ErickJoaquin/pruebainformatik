@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Registrar estudiantes</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ URL::to('/asignarAsignatura')}}">
                        {{ csrf_field() }}
                        <input type="hidden" name="idEstudiante" value="{{$idEstudiante}}">
                        <div class="form-group{{ $errors->has('asignatura') ? ' has-error' : '' }}">
                            <label for="asignatura" class="col-md-4 control-label">Asignatura</label>

                            <div class="col-md-6">
                                <select id="asignatura"  class="form-control" name="asignatura" value="{{ old('asignatura') }}" required autofocus>
                                    <option></option>
                                @foreach($asignaturas as $asignatura)
                                    <option>{{$asignatura->nombre}}</option>
                                @endForeach
                                </select>
                                @if ($errors->has('asignatura'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('asignatura') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Asignar
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
