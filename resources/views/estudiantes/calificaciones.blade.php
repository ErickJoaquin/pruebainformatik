@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Listado estudiantes</div>
                <div class="panel-body">
                    @foreach ($estudiante as $estudiante)
                    @endForeach
                    <h3>Calificaciones de: {{$estudiante->nombre}} {{$estudiante->apellido}}</h3>
                    <table class="table">
                      <thead>
                        <tr>
                          <th scope="col">Asignatura</th>
                          <th scope="col">Nota</th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach($calificacion as $calificacion)
                            <tr>
                              <td scope="row">{{$calificacion->nombre}}</td>
                              <td scope="row">{{$calificacion->calificacion}}</td>
                              <td scope="row"><a href="{{ url('/cancelarNota')}}/?idEstudiante={{$calificacion->idEstudiante}}&idAsignatura={{$calificacion->idAsignatura}}" onclick="confirm('')">Cancelar nota</a></td>
                              <td scope="row"><a href="{{ url('/cambiarNota')}}/?idEstudiante={{$calificacion->idEstudiante}}&idAsignatura={{$calificacion->idAsignatura}}">Cambiar Nota</a></td>
                            </tr>
                            <tr>
                        @endForeach
                      </tbody>
                    </table>
                    

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
