<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/registrarEstudiantes', ['middleware' => 'auth', 'uses'=>'EscuelaController@Estudiantes']);
Route::post('/registrarEstudiantes', ['middleware' => 'auth', 'uses'=>'EscuelaController@registrarEstudiantes']);
Route::get('/listarEstudiantes', ['middleware' => 'auth', 'uses'=>'EscuelaController@listarEstudiantes']);
Route::get('/asignarAsignatura', ['middleware' => 'auth', 'uses'=>'EscuelaController@asignatura']);
Route::post('/asignarAsignatura', ['middleware' => 'auth', 'uses'=>'EscuelaController@asignarAsignatura']);
Route::get('/calificaciones', ['middleware' => 'auth', 'uses'=>'EscuelaController@calificaciones']);
Route::get('/cancelarNota', ['middleware' => 'auth', 'uses'=>'EscuelaController@cancelarNota']);
Route::post('/cambiarNote', ['middleware' => 'auth', 'uses'=>'EscuelaController@cambiarNote']);
Route::get('/cambiarNota', ['middleware' => 'auth', 'uses'=>'EscuelaController@cambiarNota']);
Route::get('/asignarNota', ['middleware' => 'auth', 'uses'=>'EscuelaController@asignarNota']);
Route::post('/asignarNota', ['middleware' => 'auth', 'uses'=>'EscuelaController@asignarNote']);
Route::get('/estudianteAsignatura', ['middleware' => 'auth', 'uses'=>'EscuelaController@estudianteAsignatura']);
Route::get('/estudiantesCalificaciones','EscuelaController@estudiantesCali');
Route::post('/estudiantesCalificaciones', 'EscuelaController@estudiantesCalificaciones');